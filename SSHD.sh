#!/bin/bash
#
# SSHD.sh
# Configure SSHD 
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt

PUBKEY=0

##TODO

#Port 22
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::


#ServerKeyBits 2048

#LoginGraceTime 30s
#PermitRootLogin no
#StrictModes yes
#MaxAuthTries 3
#MaxSessions 3

#RSAAuthentication yes
#PubkeyAuthentication yes

#PasswordAuthentication no