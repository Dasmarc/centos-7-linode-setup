#!/bin/bash
#
# Firstboot.sh
# This bash script has been written to configure a
# new CentOS 7 - Linode VPS. It should be run 
# on first boot from the command line. 
#
# Script follows recommendations for 
# Red Hat as detailed in their guides
# https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/
#
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt

# version 0.1-alpha
# Configuration includes:
	# Set TimeZone
	# Install Distro Kernel
	# Set minimum password strength
	# Update system
	# Enable firewall and basic configuration
	# Provide a log of system information
	# Automate security updates


source Log.sh
source Kernel.sh
source Linode.sh
source CleanUp.sh







### Time Zone ###

# function checktimezone(){
# timedatectl set-timezone "$1" |& read && zoneoptions
# }

# function zoneoptions(){
# echo "The timezone entered in not valid. Would you like to see a list? [Y] or [N]"
# read answer;

# case $answer in
#         [Yy]* ) displayzonelist;;
#         [Nn]* ) settimezone;;
#         * ) echo "Answer not valid - Y for yes and N for No"; zoneoptions;;
# esac
# }

# function settimezone()
# {
# echo "Enter server Time Zone, followed by [ENTER]:"
# read timezone;

# checktimezone "$timezone"

# if [[ "$zoneset" = 0 ]]; then
# 	zoneset=1
# 	timedatectl status | log
# fi
# }

# function filterzonelist() {

# list=$(timedatectl list-timezones | grep "$1")

# if [ -z "$list" ]; then
# echo "No City or Country by that name found. Please try again:";
# read country;
# filterzonelist "$country"
# else
# echo "Press q to exit list"; timedatectl list-timezones | grep "$1"
# fi

# }

# function displayzonelist()
# {
# echo "If you would like to filter the list enter a city or country"
# echo "otherwise just press enter"
# read country;

# if [ -z "$country" ]; then echo "Press q to exit list"; timedatectl list-timezo$
# else  filterzonelist "$country";
# fi

# settimezone

# }


# ### Hostname ###
# function sethostname() {

# echo " "
# echo "#### Hostname ####"
# echo "A hostname is a free-form string up to 64 characters in length"
# echo "Red Hat recommends using a fully-qualified domain name"
# echo "for example -  server.example.com"
# echo "More information is available in Red Hat's Networking Guide"
# echo "Please enter a hostname:"
# read hostname

# hostnamectl set-hostname "$hostname"
# hostnamectl status
# }


# ### Network ###
# function networkingformation()
# {
# nmcli device status | log
# nmcli connection show --active | log
# ip addr show | log
# }







# function linode_json()
# {
# CURL_IMPORT="$1" python - <<END
# import os
# import json
# import decimal

# linode_json_data = os.environ['CURL_IMPORT']
# linode_data = json.loads(linode_json_data, parse_float=decimal.Decimal)

# print(json.dumps(linode_data['DATA'], indent=2))
# END
# }

# function getvalue() {
# array=("${!1}")
# #echo "${array[@]}"
# i=0
# length=${#array[@]}
# while [ "$i" -lt "$length" ]
#         do
#                 if [[ "${array[i]}" == *"$2"* ]]; then
#                         echo "${array[i+1]}"
#                         return 1
#                 fi
#         ((i++))
# done
# return 0
# }

# json=($(linode_json "$(curl --tlsv1.2 --ipv4 --keepalive-time 30 --limit-rate 10M --max-time 30 --trace "~/trace.txt" --trace-time --get --url https://api.linode.com -d api_key=XXh3sH5zPe9FeqadiNr3IOWJ0mm7Bww7VRyTJWA9Yf5kDwWl1zkc8WT6Xx3KG9RR -d api_action=linode.list -d LinodeID=954985)"))

# declare -i HD=$(getvalue var[@] "TOTALHD" | grep -o '[0-9]\+')

# if [ "$HD" -lt 24000 ]; then
#         log "Error TotalHD is too small ("${HD}"), something went wrong....exiting" 0
# fi


# function getcurrentdisks() {

# numdisk=($(ls /dev/xvd*))

# declare -i i=0
# declare -i length=${#numdisk[@]}
# declare -i add=0


# while [ $i -lt $length ]
#         do
#         echo "Disk ${i} ${numdisk[i]}"
#         add+=$(fdisk -l ${numdisk[i]} | grep -o '[0-9]\+[[:space:]]MB*' | grep -o '[0-9]\+')
#         ((i++))
# done


# }


# ### System information ###
# echo "##Initial System Information##" | log
# cat /etc/centos-release | log
# echo "##Disk Space##" | log
# df -h | log
# echo "#Networking#"
# networkingformation
# systemctl -l list-sockets | log
# echo "#List-Units#"
# systemctl -l list-units | log
# echo "#Service Status#"
# systemctl -l status | log


# ### Configure ##
# echo "##Starting configuration##" | log

#



# ## PAM ##
# setpasswordstrength;

# ## Cron ##
# autosecurityupdates;



# ### Setup ###
# settimezone
# sethostname



# ### Disable services ###
# systemctl --quiet disable iprdump iprinit iprupdate 

# #Update system
# yum list updates | log

# yum -y --quiet update && log "System Updated" 1 || log "Failure - System Update" 0;

# install_kernel();
# create_menu();

# yum clean all && log "Yum Cleaned" 1 || log "Failure - Cleaning Yum" 0;

# createmenu


main() {
#LINODE API
linode_init
linode_setup
}

main;
clean_up;
exit 0




