#!/bin/bash
#
# OSSEC.sh
# Setup and configure OSSEC
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt