#!/bin/bash
#
# Log.sh
# Very simple log script to output progress 
# and information to selected file. Causes script
# exit on script failure.
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt

source CleanUp.sh

#The location of log file - default root
LOG_LOCATION="/firstrun-log.txt";


#######################################
# Log input to LOG_LOCATION file
# Globals:
#   LOG_LOCATION
# Arguments:
#   StdIn 
#   OR 
#   String to print
#   Success boolean 0=true 
# Returns:
#   None
#######################################
function log()
{
	if [ -z "$1" ]; then
	  while read output; do echo "$output" >> "${LOG_LOCATION}"; done;
    else
	  echo "$1"
	  echo "$1" >> "${LOG_LOCATION}" 
	  if [ "$2" = 0 ]
		 then
		 echo "Install Failed - [$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
		 echo "Install Failed - [$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >> "${LOG_LOCATION}"
		 # exit on failure
		 clean_up;
		 exit 1
	  fi
	fi
}