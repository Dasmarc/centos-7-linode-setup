#!/bin/bash
#
# CleanUp.sh
# Cleanup before exit.
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt

source Firewall.sh

clean_up(){

	firewall_public;

}