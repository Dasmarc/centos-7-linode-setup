#!/bin/bash
#
# SecurityUpdates.sh
# Add security updates to cron
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt

function autosecurityupdates() {
	printf "#!/bin/bash

		loglocation=\"/var/log/yum_security\"
		date >> \"$loglocation\"
		if [[ $(yum check-update --security) = 100 ]]; then
			yum update --security >> \"$loglocation\"
		fi"  >> /etc/cron.daily/securityupdates.sh

	chmod 755 /etc/cron.daily/securityupdates.sh
}