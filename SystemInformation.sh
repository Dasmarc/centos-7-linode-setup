#!/bin/bash
#
# SystemInformation.sh
# Gather information about the current system
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt


##TODO

# echo "##Initial System Information##" | log
# cat /etc/centos-release | log
# echo "##Disk Space##" | log
# df -h | log
# echo "#Networking#"
# networkingformation
# systemctl -l list-sockets | log
# echo "#List-Units#"
# systemctl -l list-units | log
# echo "#Service Status#"
# systemctl -l status | log

# ### Network ###
# function networkingformation()
# {
# nmcli device status | log
# nmcli connection show --active | log
# ip addr show | log
# }


function ip4 {
	log "IP 4:" 0
	ip -4 address | log
}

function ip6 {
	log "IP 6:" 0
	ip -6 address | log
}