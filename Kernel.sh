#!/bin/bash
#
# Kernel.sh
# Add and configure distro kernel
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt

source Log.sh


#######################################
# Create menu.lst for pvgrub
# Globals:
#   None
# Arguments:
#   None 
# Returns:
#   None
#######################################
create_menu()
{
	# get uuid for root
	rootdevice=($(blkid -o value /dev/xvda));
    
	# pvgrub needs menu.lst
	file="/boot/grub/menu.lst"

	# backup menu.lst if it already exists
	if [ -e $file ] 
		then
		copy=$file
		copy+="-backup";
		cp $file $copy;
	fi
	
	# start with a clean menu.lst
	echo " " > $file;

	#add required details to menu.lst
    printf " default=0
    timeout=10
    splashimage=(hd0)/boot/grub/splash.xpm.gz\n
	 title CentOS 7
    root (hd0)
    kernel $(readlink -f /boot/vmlinuz-3*) ro root=UUID=${rootdevice[0]}
    initrd $(readlink -f /boot/initramfs-3*)"  >> $file
}


#######################################
# Use yum to install distro kernel
# Globals:
#   None
# Arguments:
#   None 
# Returns:
#   None
#######################################
install_kernel()
{
	yum -y --quiet install kernel && log "Distro Kernel Installed" 0 || log "Failure - Unable to install Distro Kernel" 1;
}