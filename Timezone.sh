#!/bin/bash
#
# Timezone.sh
# Configure server's time zone
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt

#global variables
ZONE_SET=0;

#######################################
# Get APIKEY from user
# Globals:
#   API_KEY
# Arguments:
#   None
# Returns:
#   None
#######################################
function checktimezone(){
timedatectl set-timezone "$1" |& read && zoneoptions
}

#######################################
# Get APIKEY from user
# Globals:
#   API_KEY
# Arguments:
#   None
# Returns:
#   None
#######################################
function zoneoptions(){
echo "The timezone entered in not valid. Would you like to see a list? [Y] or [N]"
read answer;

case $answer in
        [Yy]* ) displayzonelist;;
        [Nn]* ) settimezone;;
        * ) echo "Answer not valid - Y for yes and N for No"; zoneoptions;;
esac
}

#######################################
# Get APIKEY from user
# Globals:
#   API_KEY
# Arguments:
#   None
# Returns:
#   None
#######################################
function settimezone()
{
echo "Enter server Time Zone, followed by [ENTER]:"
read timezone;

checktimezone "$timezone"

if [[ "$ZONE_SET" = 0 ]]; then
	ZONE_SET=1
	timedatectl status | log
fi
}

#######################################
# Get APIKEY from user
# Globals:
#   API_KEY
# Arguments:
#   None
# Returns:
#   None
#######################################
function filterzonelist() {

list=$(timedatectl list-timezones | grep "$1")

if [ -z "$list" ]; then
echo "No City or Country by that name found. Please try again:";
read country;
filterzonelist "$country"
else
echo "Press q to exit list"; timedatectl list-timezones | grep "$1"
fi

}

#######################################
# Get APIKEY from user
# Globals:
#   API_KEY
# Arguments:
#   None
# Returns:
#   None
#######################################
function displayzonelist()
{
echo "If you would like to filter the list enter a city or country"
echo "otherwise just press enter"
read country;

if [ -z "$country" ]; then echo "Press q to exit list"; timedatectl list-timezo$
else  filterzonelist "$country";
fi

settimezone

}