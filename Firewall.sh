#!/bin/bash
#
# Firewall.sh
# Configure firewall
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt

# --get-active-zones
#--get-default-zone

start_firewall() {
systemctl start firewalld | log
systemctl enable firewalld | log
}

firewall_block() {
echo "The firewall will now be set to block any new connections"
echo "this will allow the script to run uninterrupted."
echo "If you are disconnected please use Linode's Lish for Remote Access."
echo "Access via ssh should be regained once the script is completed."
echo "Press return to continue:"
read cont;

firewall-cmd --set-default-zone=block;
}

firewall_public() {
	
firewall-cmd --set-default-zone=public;

}