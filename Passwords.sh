#!/bin/bash
#
# Passwords.sh
# Set PAM password quality to strength passwords
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt


function setpasswordstrength() {
echo "password   required     pam_pwquality.so retry=3" >> /etc/pam.d/passwd
echo "difok = 5 " >> /etc/security/pwquality.conf
echo "minlen = 9 " >> /etc/security/pwquality.conf
echo "minclass = 4 " >> /etc/security/pwquality.conf
echo "maxrepeat = 3 " >> /etc/security/pwquality.conf
}