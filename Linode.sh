#!/bin/bash
#
# Linode.sh
# Crude functions to provide very basic access
# to Linode's API
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt


#source
source Log.sh

#Globals
API_KEY=0
LINODE=""
LINODE_ID=0
LINODE_CONFIG=""
CURL_IMPORT=""

linode_init() {
	get_apikey;
	get_linodeid;
}

#######################################
# Get APIKEY from user
# Globals:
#   API_KEY
# Arguments:
#   None
# Returns:
#   None
#######################################
get_apikey(){
echo "Please enter your Linode APIKey and press [ENTER]:"
read API_KEY
}


#######################################
# Get LinodeID from user
# Globals:
#   LINODE_ID
# Arguments:
#   None
# Returns:
#   None
#######################################
get_linodeid(){
echo "Please enter your LinodeID and press [ENTER]:"
read LINODE_ID
}

#######################################
# Decode json response using python
# Globals:
#   CURL_IMPORT
# Arguments:
#   Encoded JSON response
# Returns:
#   Pretty JSON response
#######################################
linode_json()
{
CURL_IMPORT="$1" python - <<END
import os
import json
import decimal

linode_json_data = os.environ['CURL_IMPORT']
linode_data = json.loads(linode_json_data, parse_float=decimal.Decimal)
if not linode_data['ERRORARRAY']:
   	print(json.dumps(linode_data['DATA'], indent=2))
else:
    print(json.dumps(linode_data['ERRORARRAY'], indent=2))
END
}


#######################################
# Simple grep to extract value from JSON
# Globals:
#   none
# Arguments:
#   Pretty JSON response
#   Value to find
# Returns:
#   Full line for value
#######################################
find_json_value() {
	log "Finding JSON value=${2}" 0
	echo "${!1}" | grep "${2}"
}




#######################################
# Simple curl to retrive Linode json
# Globals:
#   API_KEY
# Arguments:
#   Api_action
#   Parameters (e.g LinodeID=739362)
# Returns:
#   Encoded json
#######################################
function linode_api()
{
local parameters=" -d api_key=${API_KEY} -d api_action=${1}"

for i do
 if [[ "$i" == *"="* ]]; then parameters+=" -d $i"; fi
done

local curl_result

curl_result="$(curl -v --get --tlsv1.2 --ipv4 --keepalive-time 30 \
	--limit-rate 10M --max-time 30 ${parameters}  https://api.linode.com)"

echo  "Curl retrieved ${curl_result}" | log

linode_list=linode_json "${curl_result}" | log

echo "${linode_list}"

}


#######################################
# Finds the current disks 
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
calculate_current_disk_usage() {

local numdisk=($(ls /dev/xvd*))

declare -i i=0
declare -i length=${#numdisk[@]}
declare -i add=0


while [ $i -lt "${length}" ]
        do
        echo "Disk ${i} ${numdisk[i]}"
        add+="$(fdisk -l "${numdisk[i]}" | grep -o '[0-9]\+[[:space:]]MB*' | grep -o '[0-9]\+')"
        ((i++))
done

echo "${add}"

}


#######################################
# Get information about Linode
# Globals:
#   LINODE_ID
# Arguments:
#   None
# Returns:
#   None
#######################################
linode() {
	LINODE=$(linode_api linode.list LinodeID="${LINODE_ID}")
	echo "Linode retrieved -> ${LINODE}"
}


#######################################
# Get the current configration for
# the Linode 
# Globals:
#   LINODE_ID
# Arguments:
#   None
# Returns:
#   None
#######################################
config() {
 LINODE_CONFIG="$(linode_api linode.config.list LinodeID="${LINODE_ID}")"
}


#######################################
# Finds current Linode config ID
# Globals:
#   LINODE_CONFIG
# Arguments:
#   None
# Returns:
#   None
#######################################
config_id() {
	if [[ -z "${LINODE_CONFIG}" ]]; then
		config
    fi
		CONFIG_ID="$(find_json_value LINODE_CONFIG 'ConfigID' | grep -o '[0-9]\+')"

	if [[ "${CONFIG_ID}" -lt 100 ]]; then
		log "Config ID error too small" 1
	fi	

	log "ConfigID: ${CONFIG_ID}" 0
}


#######################################
# Finds the current disks 
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################
available_HD() {
	local declare -i HD
	HD="$(find_json_value linode 'TOTALHD' | grep -o '[0-9]\+')"
	
	if [[ "${HD}" -lt 24000 ]]; then
        log "Error TotalHD is too small (${HD})" 1
    fi

    log "HD: ${HD}" 0
    echo ${HD}
}

#######################################
# Create a 500MB boot drive and 
# add it to configuration 
# Globals:
#   LINODE_ID
# Arguments:
#   None
# Returns:
#   None
#######################################
add_boot() {
	log "Creating Boot" 0
	#Create Boot
	local boot="$(linode_api linode.disk.create LinodeID="${LINODE_ID}" Label=Boot type=ext3 size==500)"
	declare -i boot_id
	local boot_id="$(get_value boot 'DiskID' | grep -o '[0-9]\+')"
	if [[ "${boot_id}" -lt 1000 ]]; then
        log "Error Boot drive id appears incorrect - ${boot_id}. Boot drive infromation ${boot[*]}" 1
	fi
	log "Boot drive created - ${boot}" 0

	#Get current disklist
	local disk_ids=("$(get_config_disks)")

	local disklist="${boot_id}, "
	for i in "${disk_ids[@]}"; do
        disklist+="$i,"
	done

	update_config "DiskList=${disklist}" | log

}


#######################################
# Get the current DiskIDs for the
# Linode configuration 
# Globals:
#   LINODE_CONFIG
# Arguments:
#   None
# Returns:
#   None
#######################################
get_config_disks() {
	local disk_ids="$(get_value LINODE_CONFIG 'DiskList' | grep -o '[0-9]\+')"
	echo "${disk_ids}"
}

#######################################
# Get the current DiskIDs for the
# Linode configuration 
# Globals:
#	LINODE_ID
#   LINODE_CONFIG
# Arguments:
#   String of values to update
# Returns:
#   None
#######################################
update_config() {
	local update_config="$(linode_api linode.config.update LinodeID="${LINODE_ID}" ConfigID="${LINODE_CONFIG}" "$1")"
	echo "${update_config}"
}



linode_setup() {

 linode | log
 
 #total_HD="${available_HD}"
 #used_total_HD="${calculate_current_disk_usage}" 
 #free="$(total_HD-used_total_HD)"
 #add_boot

}