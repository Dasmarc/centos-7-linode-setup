#!/bin/bash
#
# SecureRoot.sh
# Disable root login and access
# Root will be allowed using Lish
# 
# Author: Andrew Sykes <asykes@interlime.com>
# Copyright (C) 2015 Andrew Sykes, Dasmarc
# Released under the MIT license - see License.txt

#TODO

#echo hvc0 > /etc/securetty

#keep="$(grep -v root /etc/passwd)"

#echo "root:x:0:0:root:/root:/sbin/nologin" > /etc/passwd

#echo keep >> /etc/passwd
